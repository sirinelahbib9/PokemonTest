import { MutationTree, ActionTree, ActionContext } from "vuex";
import { Context as AppContext } from "@nuxt/types";
import { RootState, Pokemon } from "~/types";
import axios from "axios";

export const state = (): RootState => ({
  pokemons: [],
  pokemonsTmp: [],
  limit: 25,
  offset: 0,
  selectedTypes: [],
  selectedAbilities: [],
  types: [],
  abilities: [],
})

export const mutations: MutationTree<RootState> = {
  setPokemon(state: RootState, pokemons: Pokemon[]): void {
    state.pokemons = pokemons
  },
  addPokemon(state: RootState, pokemon: Pokemon): void {
    state.pokemons.push(pokemon)
  },
  setPokemonTmp(state: RootState, pokemons: Pokemon[]): void {
    state.pokemonsTmp = pokemons
  },
  addPokemonTmp(state: RootState, pokemon: Pokemon): void {
    state.pokemonsTmp.push(pokemon)
  },
  orderPokemons(state: RootState, orderBy): void {
    let pokemonsTemp = state.pokemons;
    switch (orderBy) {
      case 0:
        pokemonsTemp.sort((pokemonA: Pokemon, pokemonB: Pokemon) =>
          pokemonA.order > pokemonB.order ? 1 : -1
        )
        break;
      case 1:
        pokemonsTemp.sort((pokemonA: Pokemon, pokemonB: Pokemon) =>
          pokemonA.baseExperience === pokemonB.baseExperience ? 0 : (pokemonA.baseExperience < pokemonB.baseExperience ? 1 : -1)
        )
        break;
      case 2:
        pokemonsTemp.sort((pokemonA: Pokemon, pokemonB: Pokemon) =>
          pokemonA.weight === pokemonB.weight ? 0 : (pokemonA.weight < pokemonB.weight ? 1 : -1)
        )
        break;
      case 3:
        pokemonsTemp.sort((pokemonA: Pokemon, pokemonB: Pokemon) =>
          pokemonA.height === pokemonB.height ? 0 : (pokemonA.height < pokemonB.height ? 1 : -1)
        )
        break;
    }
    state.pokemons = pokemonsTemp;
  },
  nextOffset(state: RootState): void {
    state.offset += state.limit;
  },
  setOffset(state: RootState, newOffset): void {
    state.offset = newOffset;
  },
  setTypes(state: RootState, types: any[]): void {
    state.types = types
  },
  setAbilities(state: RootState, abilities: any[]): void {
    state.abilities = abilities;
  },
  addSelectedType(state: RootState, typeIndex: number): void {
    // double click to enable et disable filter option:
    if (state.selectedTypes.indexOf(typeIndex) === -1) {
      //add genre filter index to selected genre index
      state.selectedTypes.push(typeIndex)
    } else {
      // if exist: we should remove it from selected type table to disable filter
      state.selectedTypes.splice(state.selectedTypes.indexOf(typeIndex), 1)
    }
  },
  addSelectedAbility(state: RootState, abilityIndex: number): void {
    // double click to enable et disable filter option:
    if (state.selectedAbilities.indexOf(abilityIndex) === -1) {
      //add genre filter index to selected genre index
      state.selectedAbilities.push(abilityIndex)
    } else {
      // if exist: we should remove it from selected abilities table to disable filter
      state.selectedAbilities.splice(state.selectedAbilities.indexOf(abilityIndex), 1)
    }
  },
}
export const getters = {
  getPokemons: (state: RootState) => () => {
    return state.pokemons;
  }
}

interface Actions<S, R> extends ActionTree<S, R> {
  nuxtServerInit(actionContext: ActionContext<S, R>, appContext: AppContext): void
  fetchNewPokemons(actionContext: ActionContext<S, R>): void
  searchPokemon(actionContext: ActionContext<S, R>, searchKeyword: String): void
  fetchTypes(actionContext: ActionContext<S, R>): void
  fetchAbilities(actionContext: ActionContext<S, R>): void
  filterPokemons(actionContext: ActionContext<S, R>): void
}

export const actions: Actions<RootState, RootState> = {
  async nuxtServerInit({ commit }, context) {
    let pokemons: Pokemon[] = []
  },
  async fetchNewPokemons({ commit, state }) {
    //template string
    let pokemons = (await axios.get(
      `https://pokeapi.co/api/v2/pokemon/?offset=${state.offset}&limit=${state.limit}`
    )).data.results;
    commit('nextOffset')
    for (let index = 0; index < pokemons.length; index++) {
      var { data } = await axios.get(pokemons[index].url);
      pokemons[index].order = data.order;
      pokemons[index].name = data.name;
      pokemons[index].picture = data.sprites.front_default;
      pokemons[index].height = data.height;
      pokemons[index].weight = data.weight;
      pokemons[index].types = data.types;
      pokemons[index].baseExperience = data.base_experience;
      pokemons[index].abilities = data.abilities.filter(
        (ability: any) => !ability.is_hidden
      );
      pokemons[index].hiddenAbilities = data.abilities.filter(
        (ability: any) => ability.is_hidden
      );

      if (state.selectedTypes.length === 0 && state.selectedAbilities.length === 0) {
        commit("addPokemon", pokemons[index]);
      } else {
        let verified = false;
        for (let i = 0; i < state.selectedTypes.length && !verified; i++) {
          if (pokemons[index].types.findIndex((pokemonType: any) => pokemonType.type.name === state.types[state.selectedTypes[i]]) !== -1)
            verified = true;
        }
        for (let i = 0; i < state.selectedAbilities.length && !verified; i++) {
          if (pokemons[index].abilities.findIndex((pokemonAbility: any) => pokemonAbility.ability.name === state.abilities[state.selectedAbilities[i]]) !== -1)
            verified = true;

          if (pokemons[index].hiddenAbilities.findIndex((pokemonAbility: any) => pokemonAbility.ability.name === state.abilities[state.selectedAbilities[i]]) !== -1)
            verified = true;
        }
        if (verified) {
          commit("addPokemon", pokemons[index]);
        }
      }
      commit("addPokemonTmp", pokemons[index]);
    }
  },
  async searchPokemon({ commit }, searchKeyword) {
    try {
      var { data } = await axios.get(
        "https://pokeapi.co/api/v2/pokemon/" + searchKeyword
      );

      let abilities = data.abilities.filter(
        (ability: any) => !ability.is_hidden
      );
      let hiddenAbilities = data.abilities.filter(
        (ability: any) => ability.is_hidden
      );
      commit("setPokemon", [
        {
          order: data.order,
          name: data.name,
          picture: data.sprites.front_default,
          height: data.height,
          weight: data.weight,
          types: data.types,
          baseExperience: data.base_experience,
          abilities,
          hiddenAbilities
        }
      ]);
    } catch (e) {
      commit("setPokemon", []);
    }
  },

  async fetchTypes({ commit }) {
    const { data } = await axios.get("https://pokeapi.co/api/v2/type");
    const types = data.results.map((type: any) => type.name)
    commit("setTypes", types);
  },

  async fetchAbilities({ commit }) {
    const { data } = await axios.get("https://pokeapi.co/api/v2/ability");
    const abilities = data.results.map((ability: any) => ability.name)
    commit("setAbilities", abilities);
  },

  async filterPokemons({ commit, state }) {
    if (state.selectedTypes.length === 0 && state.selectedAbilities.length === 0) {
      commit('setPokemon', state.pokemonsTmp);
    } else {
      const pokemons = state.pokemonsTmp.filter(pokemon => {
        for (let index = 0; index < state.selectedTypes.length; index++) {
          if (pokemon.types.findIndex(pokemonType => pokemonType.type.name === state.types[state.selectedTypes[index]]) !== -1)
            return true;
        }
        for (let index = 0; index < state.selectedAbilities.length; index++) {
          if (pokemon.abilities.findIndex(pokemonAbility => pokemonAbility.ability.name === state.abilities[state.selectedAbilities[index]]) !== -1)
            return true;

          if (pokemon.hiddenAbilities.findIndex(pokemonAbility => pokemonAbility.ability.name === state.abilities[state.selectedAbilities[index]]) !== -1)
            return true;
        }
        return false;
      })
      commit('setPokemon', pokemons);
    }
  },
}
