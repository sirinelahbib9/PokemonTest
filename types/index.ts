export * from './state';
export interface Pokemon {
  order: Number;
  name: string;
  url: string;
  picture: string;
  height: Number;
  weight: Number;
  types: any[];
  baseExperience: Number;
  abilities: any[];
  hiddenAbilities: any[];
}
