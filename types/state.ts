import { Pokemon} from "~/types";

export interface RootState {
  pokemons : Pokemon[];
  pokemonsTmp: Pokemon[];
  offset: number;
  limit: number;
  selectedTypes: number[];
  selectedAbilities: number[];
  types: string[];
  abilities: string[];
}
